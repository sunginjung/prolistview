#proListView

##Gym Membership Management App

###I studied free online lectures to develop this app, so the structure of the app might look different from Android Studio book. However, I made sure these two features were implemented:


1. ListView
2. SQLite


###You can add new member by pressing FloatingActionButton. When existing member is pressed, you can edit them. Individual member can be deleted when he/she is pressed and editor screen is opened.


