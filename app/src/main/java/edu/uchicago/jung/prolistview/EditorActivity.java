package edu.uchicago.jung.prolistview;

import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import edu.uchicago.jung.prolistview.data.MemberContract;
import edu.uchicago.jung.prolistview.data.MemberContract.MemberEntry;
import edu.uchicago.jung.prolistview.data.MemberDbHelper;

public class EditorActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private EditText mNameEditText;
    private EditText mAgeEditText;
    private Spinner mGenderSpinner;
    private Spinner mTypeSpinner;

    private MemberDbHelper mDbHelper;

    private int mGender = 0;
    private int mType = MemberEntry.MEMBER_TYPE_YEARLY;

    private Uri mCurrentMemberUri;
    private static final int EXISTING_MEMBER_LOADER = 0;

    private String mMemberName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);

        Intent intent = getIntent();
        mCurrentMemberUri = intent.getData();
        //change the title of the view depending on whether new member or existing member
        if (mCurrentMemberUri == null) {
            setTitle("Add a Member!");
        } else {
            setTitle("Edit a Member!");
            getLoaderManager().initLoader(EXISTING_MEMBER_LOADER, null, this);
        }

        mNameEditText = (EditText) findViewById(R.id.edit_member_name);
        mAgeEditText = (EditText) findViewById(R.id.edit_member_age);
        mGenderSpinner = (Spinner) findViewById(R.id.spinner_gender);
        mTypeSpinner = (Spinner) findViewById(R.id.spinner_member_type);

        setupSpinner();


    }


    private void setupSpinner() {

        ArrayAdapter genderSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_gender_options, android.R.layout.simple_spinner_item);

        genderSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        mGenderSpinner.setAdapter(genderSpinnerAdapter);

        ArrayAdapter typeSpinnerAdapter = ArrayAdapter.createFromResource(this, R.array.array_member_type_options,
                android.R.layout.simple_spinner_item);

        typeSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        mTypeSpinner.setAdapter(typeSpinnerAdapter);

        mGenderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                if (!TextUtils.isEmpty(selection)) {
                    if (selection.equals(getString(R.string.gender_male))) {
                        mGender = MemberEntry.MEMBER_GENDER_MALE;
                    } else if (selection.equals(getString(R.string.gender_female))) {
                        mGender = MemberEntry.MEMBER_GENDER_FEMALE;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mGender = MemberEntry.MEMBER_GENDER_FEMALE;
            }
        });

        mTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                if (!TextUtils.isEmpty(selection)) {
                    if (selection.equals(getString(R.string.type_yearly))) {
                        mType = MemberEntry.MEMBER_TYPE_YEARLY;
                    } else if (selection.equals(getString(R.string.type_monthly))) {
                        mType = MemberEntry.MEMBER_TYPE_MONTHLY;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mType = MemberEntry.MEMBER_TYPE_MONTHLY;

            }
        });
    }

    private void addMember() {
        String memberName = mNameEditText.getText().toString().trim();
        int memberAge = Integer.parseInt(mAgeEditText.getText().toString());

        ContentValues values = new ContentValues();
        values.put(MemberEntry.COLUMN_MEMBER_NAME, memberName);
        values.put(MemberEntry.COLUMN_MEMBER_AGE, memberAge);
        values.put(MemberEntry.COLUMN_MEMBER_GENDER, mGender);
        values.put(MemberEntry.COLUMN_MEMBER_TYPE, mType);
        //if new member, then insert. if existing member, update the database
        if (mCurrentMemberUri == null) {
            Uri newUri = getContentResolver().insert(MemberEntry.CONTENT_URI, values);

            if (newUri != null) {
                Toast.makeText(this, "Member Added!!", Toast.LENGTH_SHORT).show();
            }
        } else {

            int rowsAffected = getContentResolver().update(mCurrentMemberUri, values, null, null);

            if (rowsAffected != 0) {
                Toast.makeText(this, "Member Edited!!", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.editor_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                addMember();
                // Exit activity
                finish();
                return true;
            case R.id.action_delete:
                deleteMember();
                finish();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        String[] projection = {
                MemberEntry._ID,
                MemberEntry.COLUMN_MEMBER_NAME,
                MemberEntry.COLUMN_MEMBER_GENDER,
                MemberEntry.COLUMN_MEMBER_AGE,
                MemberEntry.COLUMN_MEMBER_TYPE};

        return new CursorLoader(this,
                mCurrentMemberUri,
                projection,
                null,                   // No selection clause
                null,
                null);                  // Default sort order
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        //if existing member is being edited, put his/her info in the views
        if (cursor == null || cursor.getCount() < 1) {
            return;
        }
        if (cursor.moveToFirst()) {

            int nameColumnIndex = cursor.getColumnIndex(MemberContract.MemberEntry.COLUMN_MEMBER_NAME);
            int ageColumnIndex = cursor.getColumnIndex(MemberContract.MemberEntry.COLUMN_MEMBER_AGE);
            int genderColumnIndex = cursor.getColumnIndex(MemberContract.MemberEntry.COLUMN_MEMBER_GENDER);
            int typeColumnIndex = cursor.getColumnIndex(MemberContract.MemberEntry.COLUMN_MEMBER_TYPE);

            mMemberName = cursor.getString(nameColumnIndex);
            Integer memberAge = cursor.getInt(ageColumnIndex);
            mGender = cursor.getInt(genderColumnIndex);
            mType = cursor.getInt(typeColumnIndex);


            mNameEditText.setText(mMemberName);
            mAgeEditText.setText(String.valueOf(memberAge));

            switch (mGender) {
                case MemberEntry.MEMBER_GENDER_FEMALE:
                    mGenderSpinner.setSelection(1);
                    break;
                case MemberEntry.MEMBER_GENDER_MALE:
                    mGenderSpinner.setSelection(0);
                    break;
                default:
                    mGenderSpinner.setSelection(0);
                    break;
            }

            switch (mType) {
                case MemberEntry.MEMBER_TYPE_MONTHLY:
                    mTypeSpinner.setSelection(0);
                    break;
                case MemberEntry.MEMBER_TYPE_YEARLY:
                    mTypeSpinner.setSelection(1);
                    break;
                default:
                    mTypeSpinner.setSelection(0);
                    break;
            }
        }


    }

    private void deleteMember() {
        // Only perform the delete if this is an existing member.
        if (mCurrentMemberUri != null) {
            int rowsDeleted = getContentResolver().delete(mCurrentMemberUri, null, null);

            // Show a toast message depending on whether or not the delete was successful.
            if (rowsDeleted == 0) {

                Toast.makeText(this, "error deleting member",
                        Toast.LENGTH_SHORT).show();
            } else {

                Toast.makeText(this, "Member deleted!!",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onLoaderReset(Loader loader) {
//        mNameEditText.setText("");
//        mAgeEditText.setText("");
    }

}
