package edu.uchicago.jung.prolistview;


import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import edu.uchicago.jung.prolistview.data.MemberContract;

public class MainActivity extends AppCompatActivity implements
        LoaderManager.LoaderCallbacks<Cursor> {

    private static final int MEMBER_LOADER = 0;

    MemberCursorAdapter mCursorAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.fab) {
                    Intent intent = new Intent(MainActivity.this, EditorActivity.class);
                    startActivity(intent);
                }
            }
        });

        //ListView referenced
        ListView memberListView = (ListView) findViewById(R.id.list);

        //set up empty view shown when there is no member to display
        View emptyView = findViewById(R.id.empty_view);
        memberListView.setEmptyView(emptyView);

        //hook up listview with cursor adapter
        mCursorAdapter = new MemberCursorAdapter(this, null);
        memberListView.setAdapter(mCursorAdapter);

        // Setup the item click listener
        memberListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                //pass intent with Uri info to EditorActivity
                Intent intent = new Intent(MainActivity.this, EditorActivity.class);

                Uri currentMemberUri = ContentUris.withAppendedId(MemberContract.MemberEntry.CONTENT_URI, id);
                intent.setData(currentMemberUri);

                startActivity(intent);
            }
        });

        getLoaderManager().initLoader(MEMBER_LOADER, null, this);
    }


    private void deleteAllMembers() {
        int rowsDeleted = getContentResolver().delete(MemberContract.MemberEntry.CONTENT_URI, null, null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_delete_all_members:
                deleteAllMembers();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public android.content.Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] projection = {
                MemberContract.MemberEntry._ID,
                MemberContract.MemberEntry.COLUMN_MEMBER_NAME,
                MemberContract.MemberEntry.COLUMN_MEMBER_GENDER,
                MemberContract.MemberEntry.COLUMN_MEMBER_AGE,
                MemberContract.MemberEntry.COLUMN_MEMBER_TYPE};


        return new CursorLoader(this,
                MemberContract.MemberEntry.CONTENT_URI,
                projection,
                null,                   // No selection clause
                null,
                null);
    }


    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        //update cursor
        mCursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mCursorAdapter.swapCursor(null);
    }


}
