package edu.uchicago.jung.prolistview.data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by sunginjung on 4/13/17.
 */

public final class MemberContract {

    //do not allow instanciating this class
    private MemberContract(){}

    //URI for the content provider
    public static final String CONTENT_AUTHORITY = "edu.uchicago.jung";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String PATH_MEMBERS = "members";

    public static final class MemberEntry implements BaseColumns{

        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_MEMBERS);
        public static final String CONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_MEMBERS;

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_MEMBERS;

        //define static variable used throughout classes
        public static final String TABLE_NAME = "members";
        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_MEMBER_NAME = "name";
        public static final String COLUMN_MEMBER_GENDER = "gender";
        public static final String COLUMN_MEMBER_AGE = "age";
        public static final String COLUMN_MEMBER_TYPE = "type";

        public static final int MEMBER_GENDER_MALE = 0;
        public static final int MEMBER_GENDER_FEMALE = 1;
        public static final int MEMBER_TYPE_YEARLY = 0;
        public static final int MEMBER_TYPE_MONTHLY = 1;

    }
}
