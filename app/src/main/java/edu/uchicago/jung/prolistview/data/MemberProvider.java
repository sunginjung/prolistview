package edu.uchicago.jung.prolistview.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

/**
 * Created by sunginjung on 4/14/17.
 */

public class MemberProvider extends ContentProvider {
    //when the whole table is referenced
    private static final int MEMBER = 100;
    //when specific member is referenced
    private static final int MEMBER_ID = 101;
    //define Uri Matcher
    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {

        sUriMatcher.addURI(MemberContract.CONTENT_AUTHORITY, MemberContract.PATH_MEMBERS, MEMBER);
        sUriMatcher.addURI(MemberContract.CONTENT_AUTHORITY, MemberContract.PATH_MEMBERS + "/#", MEMBER_ID);
    }

    private MemberDbHelper mDbHelper;

    @Override
    public boolean onCreate() {
        mDbHelper = new MemberDbHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {

        SQLiteDatabase database = mDbHelper.getReadableDatabase();

        Cursor cursor;
        //depends on whether the whole table or individual member should be queried
        int match = sUriMatcher.match(uri);
        switch (match) {
            case MEMBER:
                cursor = database.query(MemberContract.MemberEntry.TABLE_NAME, projection, selection, selectionArgs,
                        null, null, sortOrder);
                break;
            case MEMBER_ID:
                selection = MemberContract.MemberEntry._ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId(uri)) };

                cursor = database.query(MemberContract.MemberEntry.TABLE_NAME, projection, selection, selectionArgs,
                        null, null, sortOrder);
                break;
            default:
                throw new IllegalArgumentException("Cannot query unknown URI " + uri);
        }
        //notify the change so that it doesn't need to load the data every time
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case MEMBER:
                return insertMember(uri, contentValues);
            default:
                throw new IllegalArgumentException("Insertion failed for " + uri);
        }
    }

    private Uri insertMember(Uri uri, ContentValues values) {

        String name = values.getAsString(MemberContract.MemberEntry.COLUMN_MEMBER_NAME);
        if (name == null) {
            throw new IllegalArgumentException("requires a name");
        }

        Integer age = values.getAsInteger(MemberContract.MemberEntry.COLUMN_MEMBER_AGE);
        if (age == 0) {
            throw new IllegalArgumentException("age can't be 0");
        }

        Integer gender = values.getAsInteger(MemberContract.MemberEntry.COLUMN_MEMBER_GENDER);
        if (gender != 0 && gender != 1) {
            throw new IllegalArgumentException("requires valid gender");
        }

        Integer type = values.getAsInteger(MemberContract.MemberEntry.COLUMN_MEMBER_TYPE);
        if (type != 0 && type != 1) {
            throw new IllegalArgumentException("requires valid membership type");
        }


        SQLiteDatabase database = mDbHelper.getWritableDatabase();

        long id = database.insert(MemberContract.MemberEntry.TABLE_NAME, null, values);
        if (id == -1) {
            return null;
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return ContentUris.withAppendedId(uri, id);
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String selection,
                      String[] selectionArgs) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case MEMBER:
                return updateMember(uri, contentValues, selection, selectionArgs);
            case MEMBER_ID:
                selection = MemberContract.MemberEntry._ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId(uri)) };
                return updateMember(uri, contentValues, selection, selectionArgs);
            default:
                throw new IllegalArgumentException("Update is not supported for " + uri);
        }
    }


    private int updateMember(Uri uri, ContentValues values, String selection, String[] selectionArgs) {

        if (values.containsKey(MemberContract.MemberEntry.COLUMN_MEMBER_NAME)) {
            String name = values.getAsString(MemberContract.MemberEntry.COLUMN_MEMBER_NAME);
            if (name == null) {
                throw new IllegalArgumentException("requires a member name");
            }
        }

        if (values.containsKey(MemberContract.MemberEntry.COLUMN_MEMBER_AGE)) {
            Integer age = values.getAsInteger(MemberContract.MemberEntry.COLUMN_MEMBER_AGE);
            if (age <= 0 ) {
                throw new IllegalArgumentException("requires valid age");
            }
        }

        if (values.size() == 0) {
            return 0;
        }

        SQLiteDatabase database = mDbHelper.getWritableDatabase();

        int rowsUpdated = database.update(MemberContract.MemberEntry.TABLE_NAME, values, selection, selectionArgs);

        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return rowsUpdated;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {

        SQLiteDatabase database = mDbHelper.getWritableDatabase();

        int rowsDeleted;

        final int match = sUriMatcher.match(uri);
        switch (match) {
            case MEMBER:
                rowsDeleted = database.delete(MemberContract.MemberEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case MEMBER_ID:
                selection = MemberContract.MemberEntry._ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId(uri)) };
                rowsDeleted = database.delete(MemberContract.MemberEntry.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Deletion is failed for " + uri);
        }

        if (rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return rowsDeleted;
    }

    @Override
    public String getType(Uri uri) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case MEMBER:
                return MemberContract.MemberEntry.CONTENT_LIST_TYPE;
            case MEMBER_ID:
                return MemberContract.MemberEntry.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalStateException("Unknown URI " + uri + " with match " + match);
        }
    }
}
