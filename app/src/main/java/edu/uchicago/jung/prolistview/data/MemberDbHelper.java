package edu.uchicago.jung.prolistview.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import edu.uchicago.jung.prolistview.data.MemberContract.MemberEntry;

/**
 * Created by sunginjung on 4/13/17.
 */

public class MemberDbHelper extends SQLiteOpenHelper {

    //database name
    private static final String DATABASE_NAME = "member.db";

    private static final int DATABASE_VERSION = 1;

    public MemberDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //SQL method to create table
        String SQL_CREATE_MEMBERS_TABLE = "CREATE TABLE " + MemberEntry.TABLE_NAME + "("
                + MemberEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + MemberEntry.COLUMN_MEMBER_NAME + " TEXT NOT NULL, "
                + MemberEntry.COLUMN_MEMBER_AGE + " INTEGER NOT NULL, "
                + MemberEntry.COLUMN_MEMBER_GENDER + " INTEGER NOT NULL, "
                + MemberEntry.COLUMN_MEMBER_TYPE + " INTEGER NOT NULL);";

        db.execSQL(SQL_CREATE_MEMBERS_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + MemberEntry.TABLE_NAME);
        onCreate(db);
    }
}
