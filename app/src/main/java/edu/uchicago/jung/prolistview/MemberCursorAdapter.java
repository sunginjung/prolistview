package edu.uchicago.jung.prolistview;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import edu.uchicago.jung.prolistview.data.MemberContract;

/**
 * Created by sunginjung on 4/14/17.
 */

public class MemberCursorAdapter extends CursorAdapter {

    public MemberCursorAdapter(Context context, Cursor c) {
        super(context, c, 0 );
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
    }

    //method to display database info in the listview
    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        TextView nameTextView = (TextView) view.findViewById(R.id.name);
        TextView ageTextView = (TextView) view.findViewById(R.id.age);
        TextView genderTextView = (TextView) view.findViewById(R.id.gender);
        TextView typeTextView = (TextView) view.findViewById(R.id.type);

        int nameColumnIndex = cursor.getColumnIndex(MemberContract.MemberEntry.COLUMN_MEMBER_NAME);
        int ageColumnIndex = cursor.getColumnIndex(MemberContract.MemberEntry.COLUMN_MEMBER_AGE);
        int genderColumnIndex = cursor.getColumnIndex(MemberContract.MemberEntry.COLUMN_MEMBER_GENDER);
        int typeColumnIndex = cursor.getColumnIndex(MemberContract.MemberEntry.COLUMN_MEMBER_TYPE);

        String memberName = cursor.getString(nameColumnIndex);
        Integer memberAge = cursor.getInt(ageColumnIndex);
        Integer memberGender = cursor.getInt(genderColumnIndex);
        Integer memberType = cursor.getInt(typeColumnIndex);

        nameTextView.setText(memberName);
        if (memberGender == MemberContract.MemberEntry.MEMBER_GENDER_MALE){
            genderTextView.setText("MALE");
        }else {
            genderTextView.setText("FEMALE");
        }
        if (memberType == MemberContract.MemberEntry.MEMBER_TYPE_MONTHLY){
            typeTextView.setText("MONTHLY");
        } else {
            typeTextView.setText("YEARLY");
        }
        ageTextView.setText(String.valueOf(memberAge));
    }
}
